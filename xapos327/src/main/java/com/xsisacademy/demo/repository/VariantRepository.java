package com.xsisacademy.demo.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.demo.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long> {

	 //java class
	List<Variant> findByVariantActive(Boolean variantActive);
	
	@Query(value="select v from Variant v where v.variantActive = true and v.categoryId= ?1")
	List<Variant> findByCategoryId(Long categoryId);
}
