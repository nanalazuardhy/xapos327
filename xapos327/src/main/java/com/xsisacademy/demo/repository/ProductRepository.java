package com.xsisacademy.demo.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.xsisacademy.demo.model.Product;
public interface ProductRepository extends JpaRepository<Product, Long> {
	
	List<Product> findByIsActive (Boolean isActive);	
}
