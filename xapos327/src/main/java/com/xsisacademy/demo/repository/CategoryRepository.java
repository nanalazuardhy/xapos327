package com.xsisacademy.demo.repository;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.demo.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {
	
	
	@Query(value= "SELECT c.* FROM category c WHERE is_active = true ORDER BY category_name", nativeQuery = true)//query
	//@Query(value = "SELECT c FROM Category c where c.isActive=true") //java class
	List<Category> findByCategories();
	
	//PAGING
	@Query(value = "select * from category where lower(category_name) like lower(concat('%', ?1, '%')) and is_active = true order by category_name asc", nativeQuery = true)
	Page<Category> findByIsActive(String Keyword, Boolean isActive, Pageable page);
	

	
	//PAGING
	@Query(value = "select * from category where lower(category_name) like lower(concat('%', ?1, '%')'%?1%') and is_active = true order by category_name desc", nativeQuery = true)
	Page<Category> findByIsActiveDESC(String Keyword, Boolean isActive, Pageable page);
}
