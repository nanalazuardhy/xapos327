package com.xsisacademy.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.demo.model.OrderHeaders;

public interface OrderHeadersRepository extends JpaRepository<OrderHeaders, Long>{
	
	@Query(value="select max(o.id) from OrderHeaders o")
	public Long findByMaxId();
}
