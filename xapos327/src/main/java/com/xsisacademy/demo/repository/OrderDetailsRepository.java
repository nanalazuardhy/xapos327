package com.xsisacademy.demo.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.xsisacademy.demo.model.OrderDetails;

public interface OrderDetailsRepository extends JpaRepository<OrderDetails, Long>{
	
	List<OrderDetails> findOrderByHeaderId(Long id);
}
