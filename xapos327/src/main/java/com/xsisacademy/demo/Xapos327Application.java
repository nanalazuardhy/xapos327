package com.xsisacademy.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xapos327Application {

	public static void main(String[] args) {
		SpringApplication.run(Xapos327Application.class, args);
	}

}
