package com.xsisacademy.demo.controller;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.demo.model.Variant;
import com.xsisacademy.demo.repository.CategoryRepository;
import com.xsisacademy.demo.repository.VariantRepository;

@RestController
@RequestMapping("/api/")
public class ApiVariantController {
	@Autowired
	private VariantRepository variantRepository;
	@Autowired
	public CategoryRepository categoryRepository;

	@GetMapping("allVariant")
	public ResponseEntity<List<Variant>> getAllVariant() {
		try {
			
			List<Variant> listVariant = this.variantRepository.findByVariantActive(true);
			return new ResponseEntity<>(listVariant, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("variant/add")
    public ResponseEntity<Object> saveVariant(@RequestBody Variant variant){ //melempar form data jadi pake req body
    	variant.variantCreatedBy = "admin1";
    	variant.variantCreateDate = new Date();
    	Variant variantData = this.variantRepository.save(variant);
    	
    	if(variantData.equals(variant)) {
    		return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
    	}
    	else {
    		return new ResponseEntity<>("Save failed.", HttpStatus.BAD_REQUEST);
    	}
    	
    }
    
    @GetMapping("variant/{id}")
    public ResponseEntity<Object> getVariantById(@PathVariable("id") Long id){
    	try {
    		System.out.println(id);
    		Optional<Variant> variant = this.variantRepository.findById(id);
    		return new ResponseEntity<>(variant, HttpStatus.OK);
    		
    	}
    	catch(Exception e) {
    		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    	}
    }
    
    @PutMapping("variant/edit/{id}")
    public ResponseEntity<Object> editVariant(@PathVariable("id") Long id, @RequestBody Variant variant)
    {
    	
    	Optional<Variant> variantData = this.variantRepository.findById(id);   
    
    if(variantData.isPresent()) {
    	variant.id = id;
    	variant.variantModifyBy = "admin1";
    	variant.variantModifyDate = new Date();
    	variant.variantCreatedBy = variantData.get().variantCreatedBy;
    	variant.variantCreateDate = variantData.get().variantCreateDate;
    	
    	this.variantRepository.save(variant);
		return new ResponseEntity<>("Delete success!", HttpStatus.OK);
}
    
    else {
    	return ResponseEntity.notFound().build();
	}    
    }
    
    @PutMapping("variant/delete/{id}")
    public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id){
    	Optional<Variant> variantData =this.variantRepository.findById(id);
    	
    	if(variantData.isPresent()) {
    		Variant variant = new Variant();
    		variant.id = id;
    		variant.variantActive = false;
    		variant.variantModifyBy = "admin1";
    		variant.variantCreatedBy = variantData.get().variantCreatedBy;
    		variant.categoryId = variantData.get().categoryId;
    		variant.variantCreateDate = variantData.get().variantCreateDate;
    		variant.variantInitial = variantData.get().variantInitial;
    		variant.variantName = variantData.get().variantName;
    		
    		this.variantRepository.save(variant);
    		return new ResponseEntity<>("Delete success!", HttpStatus.OK);
    	}
    	else {
    		return ResponseEntity.notFound().build();
    	}
  
    }

}
