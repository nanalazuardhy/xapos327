package com.xsisacademy.demo.controller;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.demo.model.Category;
import com.xsisacademy.demo.repository.CategoryRepository;


@Controller
@RequestMapping("/category/")
public class CategoryController {
	
	@Autowired
	private CategoryRepository categoryRepository;

    @GetMapping("index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("category/index.html");
        
        //ambil data dari database
        List<Category> listCategory = this.categoryRepository.findAll();
        
        //lempar data ke view
        view.addObject("listCategory", listCategory);
        return view;
    }
    
    @GetMapping("addform")
    public ModelAndView addForm() {
        ModelAndView view = new ModelAndView("category/addform.html");
        
        Category category = new Category();
        view.addObject("category",category); 
        
        return view;
    }
    
    @PostMapping("save")
    public ModelAndView save(@ModelAttribute Category category, BindingResult result) {
        if (!result.hasErrors()) {
            if (category.id == null) {
               
                category.setCreatedBy("admin1");
                category.setCreatedDate(new Date());
            } else {
               
                Category tempCategory = this.categoryRepository.findById(category.getId()).orElse(null);
                if (tempCategory != null) {
                   
                    tempCategory.setCategoryName(category.getCategoryName());
                    tempCategory.setCategoryInitial(category.getCategoryInitial());
                    tempCategory.setIsActive(category.getIsActive());
                    tempCategory.setModifyBy("admin1");
                    tempCategory.setModifyDate(new Date());
                    category = tempCategory;
                }
            }

           
            this.categoryRepository.save(category);
        } else {
         
            return new ModelAndView("redirect:/category/index");
        }
        return new ModelAndView("redirect:/category/index");
    }
    
    @GetMapping("edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
    	ModelAndView view = new ModelAndView("category/addform");
    	
    	Category category = this.categoryRepository.findById(id).orElse(null);
    	view.addObject("category", category);
    	return view;
    	}
    @GetMapping("delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
    	ModelAndView view = new ModelAndView("category/addform");
    	
    	Category category = this.categoryRepository.findById(id).orElse(null);
    	
    	if(category != null) {
    		this.categoryRepository.delete(category);
    	}
        return new ModelAndView("redirect:/category/index"); 
        
    }
    
    // USING API 
    @GetMapping("indexapi")
    public ModelAndView indexapi() {
    	ModelAndView view = new ModelAndView("category/indexapi.html");
    	return view;
    }
    
    // USING API 
    @GetMapping("indexapi_pg")
    public ModelAndView indexapi_pg() {
    	ModelAndView view = new ModelAndView("category/indexapi_pg.html");
    	return view;
    }
    
}


