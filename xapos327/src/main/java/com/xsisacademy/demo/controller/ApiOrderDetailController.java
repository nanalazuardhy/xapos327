package com.xsisacademy.demo.controller;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.demo.model.OrderDetails;
import com.xsisacademy.demo.repository.OrderDetailsRepository;
import com.xsisacademy.demo.repository.ProductRepository;

@RestController
@RequestMapping("/api/transaction/")
public class ApiOrderDetailController {
	
	@Autowired
	private OrderDetailsRepository orderDetailsRepository;
	
	@Autowired
	private ProductRepository productRepository;

	@PostMapping("orderdetails/add")
	public ResponseEntity<Object> saveOrderDetails(@RequestBody OrderDetails orderDetails){
		orderDetails.isActive = true;
		orderDetails.createBy = "admin1";
		orderDetails.createDate = new Date();
		
		OrderDetails orderDetailsData = this.orderDetailsRepository.save(orderDetails);
		
		if(orderDetailsData.equals(orderDetails)) {
			return new ResponseEntity<>("Save Item Success", HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Save Failed", HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("orderdetailbyheaderid/{headerId}")
	public ResponseEntity<List<OrderDetails>> getOrderByHeaderId(@PathVariable("headerId") Long id){
		try {
			List<OrderDetails> orderdetails = this.orderDetailsRepository.findOrderByHeaderId(id);
			return new ResponseEntity<>(orderdetails, HttpStatus.OK);
		}
		catch (Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	
}
