package com.xsisacademy.demo.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.demo.model.OrderDetails;
import com.xsisacademy.demo.model.OrderHeaders;
import com.xsisacademy.demo.model.Product;
import com.xsisacademy.demo.repository.OrderDetailsRepository;
import com.xsisacademy.demo.repository.OrderHeadersRepository;
import com.xsisacademy.demo.repository.ProductRepository;

@RestController
@RequestMapping("/api/transaction/")
public class ApiOrderHeaderController {
	@Autowired
	private OrderDetailsRepository orderDetailsRepository;
	
	@Autowired
	private OrderHeadersRepository orderHeadersRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@PostMapping("orderheader/create")
	public ResponseEntity<Object> createReference(){
		OrderHeaders orderHeaders = new OrderHeaders();
		
		String timeDec = String.valueOf(System.currentTimeMillis());
		
		orderHeaders.reference = timeDec;
		orderHeaders.amount = 0;
		orderHeaders.active = true;
		orderHeaders.createBy= "admin1";
		orderHeaders.createDate = new Date();
		
		OrderHeaders orderHeaderData = this.orderHeadersRepository.save(orderHeaders);
		
		if(orderHeaderData.equals(orderHeaders)) {
			return new ResponseEntity<>("Create Success", HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Create Success", HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping("maxorderheaderid")
	public ResponseEntity<Long> getMaxOrderHeaders(){
		try {
			Long maxId = this.orderHeadersRepository.findByMaxId();
			return new  ResponseEntity<>(maxId, HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);			
		}
	}

	@PutMapping("checkout/{headerId}/{totalAmount}")
	public ResponseEntity<Object> checkout(@PathVariable("headerId") Long id, @PathVariable("total_amount") double totalAmount){
		try {
			
			OrderHeaders orderHeaderData = this.orderHeadersRepository.findById(id).orElse(null);
			orderHeaderData.amount = totalAmount;
			orderHeaderData.modifyBy = "admin1";
			orderHeaderData.modifyDate = new Date();
			
			List<OrderDetails> orderDetails = this.orderDetailsRepository.findOrderByHeaderId(id);
			
			for(OrderDetails item : orderDetails) {
				Product product = item.product;
				product.stock = item.qty;
				
				this.productRepository.save(product);
			}
			
			this.orderHeadersRepository.save(orderHeaderData);
			return new ResponseEntity<>("Checkout success", HttpStatus.OK);
			
		}
		catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);	
		}
	}
}
