package com.xsisacademy.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.demo.model.Category;
import com.xsisacademy.demo.model.Variant;
import com.xsisacademy.demo.repository.CategoryRepository;
import com.xsisacademy.demo.repository.VariantRepository;

@Controller
@RequestMapping("/variant/")
public class VariantController {

	@Autowired
	public VariantRepository variantRepository;
	@Autowired
	public CategoryRepository categoryRepository;

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("variant/index");

		// Retrieve data from the database
		List<Variant> listVariant = variantRepository.findAll();

		// Add data to the view
		view.addObject("listVariant", listVariant);
		return view;
	}

	@GetMapping("addform")
	public ModelAndView addForm() {
		ModelAndView view = new ModelAndView("variant/addform.html");

		Variant variant = new Variant();
		view.addObject("variant", variant);

		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategory);

		return view;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Variant variant, BindingResult result) {
		ModelAndView view = new ModelAndView("variant/addform");

		if (!result.hasErrors()) {
			if (variant.getId() == null) {
				variant.setVariantCreatedBy("admin1");
				variant.setVariantCreateDate(new Date());
			} else {
				Variant tempVar = variantRepository.findById(variant.getId()).orElse(null);
				if (tempVar != null) {
					tempVar.setVariantName(variant.getVariantName());
					tempVar.setVariantInitial(variant.getVariantInitial());
					tempVar.setVariantActive(variant.getVariantActive());
					tempVar.setVariantModifyBy("admin1");
					tempVar.setVariantModifyDate(new Date());

				}
			}
			this.variantRepository.save(variant);
			return new ModelAndView("redirect:/variant/index");
		}
		else {
		return new ModelAndView("redirect:/variant/index");
		}
	}

	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("variant/addform"); 
		
		Variant variant = this.variantRepository.findById(id).orElse(null);
		view.addObject("variant", variant);
		
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategory);
		
		return view;
	}

	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		Variant variant = variantRepository.findById(id).orElse(null);

		if (variant != null) {
			variantRepository.delete(variant);
		}

		return new ModelAndView("redirect:/variant/index");
	}
	// USING API 
    @GetMapping("indexapi")
    public ModelAndView indexapi() {
    	ModelAndView view = new ModelAndView("variant/indexapi.html");
    	return view;
    }
}

