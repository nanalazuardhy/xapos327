package com.xsisacademy.demo.model;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="order_details")
public class OrderDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long id;
	
	@Column(name = "header_id")
	public Long headerId;
	
	@Column(name ="product_id")
	public Long productId;
	
	@Column(name = "qty")
    public Double qty;
	
	@Column(name = "price")
    public Double price;

	@Column(name = "is_active")
    public Boolean isActive;
	
	@Column(name = "create_by")
    public String createBy;
	
	@Column(name = "create_date")
    public Date createDate;
	
	@Column(name = "modify_by")
    public String modifyBy;
	
	@Column(name = "modify_date")
    public Date modifyDate;
	
	@ManyToOne
	@JoinColumn(name="header_id", insertable=false, updatable=false)
	public OrderHeaders header;
	
	@ManyToOne
	@JoinColumn(name="product_id", insertable=false, updatable=false)
	public Product product;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getHeaderId() {
		return headerId;
	}

	public void setHeaderId(Long headerId) {
		this.headerId = headerId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public OrderHeaders getHeader() {
		return header;
	}

	public void setHeader(OrderHeaders header) {
		this.header = header;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	
}
