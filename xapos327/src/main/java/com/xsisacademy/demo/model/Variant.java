package com.xsisacademy.demo.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "variant")
public class Variant {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;
	
	 @Column(name = "category_id")
	 public Long categoryId;

    public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name = "variant_initial")
    public String variantInitial;

    @Column(name = "variant_name")
    public String variantName;

    @Column(name = "variant_active")
    public Boolean variantActive;

    @Column(name = "variant_createdby")
    public String variantCreatedBy;

    @Column(name = "variant_createdate")
    public Date variantCreateDate; 

    @Column(name = "variant_modifyby")
    public String variantModifyBy;

    @Column(name = "variant_modifydate")
    public Date variantModifyDate;

    public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@ManyToOne
    @JoinColumn(name = "category_id", insertable = false, updatable = false)
    public Category category;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVariantInitial() {
		return variantInitial;
	}

	public void setVariantInitial(String variantInitial) {
		this.variantInitial = variantInitial;
	}

	public String getVariantName() {
		return variantName;
	}

	public void setVariantName(String variantName) {
		this.variantName = variantName;
	}

	public Boolean getVariantActive() {
		return variantActive;
	}

	public void setVariantActive(Boolean variantActive) {
		this.variantActive = variantActive;
	}

	public String getVariantCreatedBy() {
		return variantCreatedBy;
	}

	
	public void setVariantCreatedBy(String variantCreatedBy) {
		this.variantCreatedBy = variantCreatedBy;
	}

	public Date getVariantCreateDate() {
		return variantCreateDate;
	}

	public void setVariantCreateDate(Date variantCreateDate) {
		this.variantCreateDate = variantCreateDate;
	}

	public String getVariantModifyBy() {
		return variantModifyBy;
	}

	public void setVariantModifyBy(String variantModifyBy) {
		this.variantModifyBy = variantModifyBy;
	}

	public Date getVariantModifyDate() {
		return variantModifyDate;
	}

	public void setVariantModifyDate(Date variantModifyDate) {
		this.variantModifyDate = variantModifyDate;
	} 

   
}


